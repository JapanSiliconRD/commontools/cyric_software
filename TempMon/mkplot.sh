#! /bin/bash

# need to mount shared folder by using following command 
# do this as super user
# mount -t cifs  //192.168.253.166/cyric /win/cyric

source /usr/local/ROOT/v5.34.38/bin/thisroot.sh


usage() {
        echo "Usage: $0 [-l num | -r runnumber] [-s starttime] [-M minute ] [-H hours] [-h] [-v]"
	echo "         -o           : offline mode"
	echo "         -l num       : draw last [num] files"
	echo "         -r runnumber : draw one file with [runnumber]"
	echo "         -s starttime : e.g. 20150213101520"
	echo "         -M minute    : draw only last [minutes] minutes"
	echo "         -H hours     : draw only last [hours] hours"
}
NFILES=0
RUNNUMBER=0
PLOTRANGE=-1
STARTTIME=-1
OFFLINE=0;
while getopts ol:r:s:M:H:hv OPT
do
    case $OPT in
        o)  OFFLINE=1   ;;
        l)  NFILES=$OPTARG   ;;
        r)  RUNNUMBER=$OPTARG   ;;
	s)  STARTTIME=$OPTARG   ;;
        M)  PLOTRANGE=$OPTARG   ;;
        H)  PLOTRANGE=`echo "60 * $OPTARG" | bc -l`   ;;
        h)  usage
	    exit   ;;
        v)  echo "$VERSION"
	    exit   ;;
    esac
done

if [ $RUNNUMBER -eq 0 ]
then
    if [ $NFILES -eq 0 ]
    then
	usage
	exit
    else
	RUNNUMBER=`echo "$NFILES * -1" | bc -l `
    fi
fi


 ./killmkplot.sh

 echo root -l "mkplot.C(${RUNNUMBER},${PLOTRANGE},${STARTTIME},${OFFLINE})"
 root -l "mkplot.C(${RUNNUMBER},${PLOTRANGE},${STARTTIME},${OFFLINE})" >& log.txt &

 #./killmkplot.sh
