import math
#import RPi.GPIO as GPIO
#import time
#import smbus

#unit
kOhm=1000
K=273.16
mV=0.001

def RtoT(R):
#    B=3435.
#    T0=25.+K
#    R0=10.*kOhm
    B=3398.23
    T0=-10.+K
    R0=44.72*kOhm


    T=1./((1/B)*math.log(R/R0,math.e)+1./T0)
    return T-K
    
def VtoT(Vmes):
    R4=39000.
    R5=4700.
#    R7=10000.
    R7=51000.
    Vref=2.5
    R=R4/(1./(R7/(R5+R7)-Vmes/Vref)-1)
    return RtoT(R)

def setres():
    deglist=[]
    for  ind in range(20):
        ii=-40+ind*10
        deglist.append(ii+K);
        if ii==20 :
            deglist.append(25+K);
        if ii==80 :
            deglist.append(85+K);
        if ii==120 :
            deglist.append(125+K);
  
    kohmlist=[]
    kohmlist.append(221.9)
    kohmlist.append(125.1)
    kohmlist.append(73.38)
    kohmlist.append(44.72)
    kohmlist.append(28.16)
    kohmlist.append(18.25)
    kohmlist.append(12.14)
    kohmlist.append(10.00)
    kohmlist.append(8.283)
    kohmlist.append(5.781)
    kohmlist.append(4.120)
    kohmlist.append(2.996)
    kohmlist.append(2.214)
    kohmlist.append(1.665)
    kohmlist.append(1.451)
    kohmlist.append(1.271)
    kohmlist.append(0.9832)
    kohmlist.append(0.7707)
    kohmlist.append(0.6114)
    kohmlist.append(0.5469)
    return deglist,kohmlist

# test from actual measurement print(round(VtoT(12*mV),2)
debug=1
if debug :
    print(round(VtoT(70*mV),2))
    print(round(VtoT(100*mV),2))
    print(round(VtoT(180*mV),2))
    print(round(VtoT(200*mV),2))
    print(round(VtoT(240*mV),2))
    print(round(VtoT(300*mV),2))
    print(round(VtoT(360*mV),2))
    print(round(VtoT(410*mV),2))
    print(round(VtoT(490*mV),2))
    print(round(VtoT(610*mV),2))
    print(round(VtoT(1800*mV),2))
    

    deglist,kohmlist=setres()

    print(deglist[3]-K, kohmlist[3])

    print("resistivity[kOhm]", ", degree", ", degree from calc")
    for ii in range(20):
        print(kohmlist[ii], deglist[ii]-K, round(RtoT(kohmlist[ii]*kOhm),2))


        
