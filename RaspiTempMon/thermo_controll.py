#! /usr/bin/env /usr/bin/python
import os
import RPi.GPIO as GPIO
import time
import smbus
from datetime import datetime
from pytz import timezone
from ntccalc2 import VtoT2
from ntccalc import VtoT
from filehandle import getnewlogfile,appendlogtofile

#os.system("source i2cget.sh")
os.system("sudo i2cget -y 1 0x35 0xDF")

#settemp=-38.5 # target temp
#settemp=-15 # target temp
settemp=-15 # target temp
twidth=0.5   # target temp width
SW1 = 4  # GPIO pin number
SW2 = 15 # GPIO pin number

# select type of ADC
ADCTYPE="MAX1238"  # 12bit 4.096V scale
#ADCTYPE="MAX1138"   # 10bit 4.096V scale
#ADCTYPE="MAX1038"  #  8bit 4.096V scale

if ADCTYPE == "MAX1238" :
    BITMASK=240
    BITSCALE=1
elif ADCTYPE == "MAX1138" :
    BITMASK=252
    BITSCALE=4
elif ADCTYPE == "MAX1038" :
    BITMASK=256
    BITSCALE=16

#current_trh = "/var/samba/trh/temp.dat"
#current_trh = "data/temp.dat"

# unit define
mV=0.001

# initialize pins and I2C
GPIO.setmode(GPIO.BCM)
GPIO.setup(SW1, GPIO.OUT) 
GPIO.setup(SW2, GPIO.OUT)
i2c_channel = 1
i2c = smbus.SMBus(i2c_channel)
status = 1

# initial valve condition
GPIO.output(SW1,GPIO.LOW)  
GPIO.output(SW2,GPIO.HIGH)
SW1isHigh=0
SW2isHigh=1

fflog=getnewlogfile()
with open(fflog,"w"):
    pass
index=0
pretemp1=0
pretemp2=0

while True:
    isCorrectData=False
    # getting temperature
    raw1 = i2c.read_i2c_block_data(0x35,0x61,0x02) #NTC1 binary volt
    raw2 = i2c.read_i2c_block_data(0x35,0x67,0x02) #NTC2 binary volt
    raw3 = i2c.read_i2c_block_data(0x35,0x6D,0x02) #HIH1 binary volt    
    raw4 = i2c.read_i2c_block_data(0x35,0x6F,0x02) #HIH2 binary volt
    volt1 = ((raw1[0]-BITMASK)*256+raw1[1])*BITSCALE #NTC1 volt
    volt2 = ((raw2[0]-BITMASK)*256+raw2[1])*BITSCALE #NTC2 volt
    volt3 = ((raw3[0]-BITMASK)*256+raw3[1])*BITSCALE #HIH1 volt
    volt4 = ((raw4[0]-BITMASK)*256+raw4[1])*BITSCALE #HIH2 volt

#    print volt1
#    print volt2
    if  volt1 > 2000 :
        utc_now=datetime.now(timezone("UTC"))
        jst_now = utc_now.astimezone(timezone("Asia/Tokyo"))
        print "temp1 is higher than 45oC"+jst_now.strftime("%Y-%m-%d %H:%M:%S")
        time.sleep(2)
        volt1 = 2000
        #        continue;
    if  volt2 > 2000 :
        utc_now=datetime.now(timezone("UTC"))
        jst_now = utc_now.astimezone(timezone("Asia/Tokyo"))
        print "temp2 is higher than 45oC"+jst_now.strftime("%Y-%m-%d %H:%M:%S")
        time.sleep(2)
        volt2=2000
#        continue;
    
    temp1=VtoT(volt1*mV)
    temp2=VtoT(volt2*mV)
    hum1=30*volt4*mV-25
    #hum1=(volt3*mV/(4.74*0.0062)-0.16/0.0062)/(1.0546-0.00216*temp1)
    hum2=(volt4*mV/(4.74*0.0062)-0.16/0.0062)/(1.0546-0.00216*temp2)
    #hum2=30*volt4*mV-25
#    if (abs(pretemp1-temp1)+abs(pretemp2-temp2))<3 :
    if (abs(pretemp2-temp2))<2 :
        isCorrectData=True

#    hum2=
    ctrltmp=temp2
#    ctrltmp=temp1
    
    pretemp1=temp1
    pretemp2=temp2
    # getting time
    utc_now=datetime.now(timezone("UTC"))
    jst_now = utc_now.astimezone(timezone("Asia/Tokyo"))
    jst_now_out=jst_now.strftime("%Y%m%d %H%M%S")

#    print ">>> top=",round(temp1,1),"C ",round(hum1,1),"% Tbot=",round(temp2,1),"C ",round(hum2,1),"% SW1=",SW1isHigh,"SW2=",SW2isHigh,"(cooling)",jst_now.strftime("%Y-%m-%d %H:%M:%S");

    if isCorrectData :
        if ctrltmp < settemp-twidth:
            trend = "(warming)"
            GPIO.output(SW1,GPIO.HIGH)
            GPIO.output(SW2,GPIO.LOW)
            SW1isHigh=1;
            SW2isHigh=0;
            #print volt1;
            #print volt2;
            #print "top=",round(temp1,1),"C ",round(hum1,1),"% Tbot=",round(temp2,1),"C ",round(hum2,1),"% SW1=",SW1isHigh,"SW2=",SW2isHigh,"(warming)",jst_now.strftime("%Y-%m-%d %H:%M:%S")
        elif ctrltmp > settemp+twidth:
            trend = "(cooling)"
            GPIO.output(SW1,GPIO.LOW)
            GPIO.output(SW2,GPIO.HIGH)
            SW1isHigh=0;
            SW2isHigh=1;
            #print "top=",round(temp1,1),"C ",round(hum1,1),"% Tbot=",round(temp2,1),"C ",round(hum2,1),"% SW1=",SW1isHigh,"SW2=",SW2isHigh,"(cooling)",jst_now.strftime("%Y-%m-%d %H:%M:%S");
        else:
            trend = "(stable)"
            #GPIO.output(SW1,GPIO.LOW)
            #GPIO.output(SW2,GPIO.HIGH)
            SW1isHigh=0;
            SW2isHigh=0;
        data_line = "top=" + str(round(temp1,1)) + "C " + str(round(hum1,1)) + "% Tbot=" + str(round(temp2,1)) + "C " + str(round(hum2,1)) + "% SW1=" + str(SW1isHigh) + " SW2=" + str(SW2isHigh) + " " + trend + " " + jst_now.strftime("%Y-%m-%d %H:%M:%S")
        print data_line

#        try:
#            f_current = open(current_trh, "w")
#            f_current.write(data_line + "\n")
#            f_current.close()
#        except:
#            print "Error: the temp file was not opened. Skip recording the temperature."
            
        if index%1==0 :
            appendlogtofile(fflog,str(index)+" "+str(round(temp1,3))+" "+str(round(hum1,3))+" "+str(round(temp2,3))+" "+str(round(hum2,3))+" "+str(SW2isHigh)+" "+jst_now_out+"\n")
      
    index+=1
    GPIO.setup(SW1, GPIO.OUT) 
    GPIO.setup(SW2, GPIO.OUT)
    time.sleep(2)
    
        
        
    



