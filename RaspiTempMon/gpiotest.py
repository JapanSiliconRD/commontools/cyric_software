#! /usr/bin/env python
import commands
import RPi.GPIO as GPIO
import time

#GPIO.cleanup()

gpionum=4

check = commands.getoutput("gpio readall")
print "initial state"
print check
#GPIO.setmode(GPIO.BOARD)
GPIO.setmode(GPIO.BCM)
GPIO.setup(gpionum, GPIO.OUT)
#GPIO.output(7, GPIO.HIGH)
check = commands.getoutput("gpio readall")
print "set GPIO",gpionum," OUT"
print check

GPIO.setup(gpionum, GPIO.OUT)
GPIO.output(gpionum, GPIO.HIGH)
check = commands.getoutput("gpio readall")
print "set GPIO",gpionum," HIGH"
print check

time.sleep(5)
GPIO.setup(gpionum, GPIO.OUT)
GPIO.output(gpionum,GPIO.LOW)
check = commands.getoutput("gpio readall")
print "set GPIO",gpionum," LOW"
print check

