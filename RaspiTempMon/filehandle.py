import os

def updaterun() :
    finname="data/currentrun.txt"
    if not os.path.isfile(finname):
        with open(finname,mode="w") as fin:
            fin.write("2")
        print "file : ",finname," not found..."
        print "creating new file and starting from run 1"
        return "1"
    else :
        with open(finname,mode="r") as fin:
            crun=fin.read().strip()
        with open(finname,mode="w") as fin:
            fin.write(str(int(crun)+1))
        return crun

def getnewlogfile():
    runnum=updaterun()
    for ii in range(5-len(runnum)):
        runnum="0"+runnum
    fname="data/templog_run"+runnum+".txt"
    return fname


def opennewlogfile():
    fname=getlogfile()
    if os.path.isfile(fname):
        print "file exists :",fname
        return -1
    else :
        f=open(fname,mode='w')
        print fname, " opened..."
        return f

def appendlogtofile(fname,ss):
    if not os.path.isfile(fname):
        print "file : ",fname," no such file..."
    else:
        with open(fname,mode="a") as fin:
            fin.write(ss)
            fin.close()
        



#f=openlogfile()
#f.write("new line")
#f.close()
